# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Nathan Follens <nthn@unseen.is>, 2019.
msgid ""
msgstr "Project-Id-Version: PACKAGE VERSION\nReport-Msgid-Bugs-To: \nPOT-Creation-Date: 2018-09-01 20:55+0000\nPO-Revision-Date: 2019-02-10 12:09+0000\nLast-Translator: Nathan Follens <nthn@unseen.is>\nLanguage-Team: Dutch <https://hosted.weblate.org/projects/f-droid/repomaker-javascript/nl/>\nLanguage: nl\nMIME-Version: 1.0\nContent-Type: text/plain; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n != 1;\nX-Generator: Weblate 3.5-dev\n"

#, javascript-format
msgid "Available %s"
msgstr "Beschikbare %s"

#, javascript-format
msgid "This is the list of available %s. You may choose some by selecting them in the box below and then clicking the \"Choose\" arrow between the two boxes."
msgstr "Dit is de lijst met beschikbare %s. U kunt er enkele kiezen door ze hieronder te selecteren, en vervolgens te klikken op de pijl ‘Kiezen’, tussen de twee vakken."

#, javascript-format
msgid "Type into this box to filter down the list of available %s."
msgstr "Typ in dit vak om de lijst met beschikbare %s te filteren."

msgid "Filter"
msgstr "Filter"

msgid "Choose all"
msgstr "Alles kiezen"

#, javascript-format
msgid "Click to choose all %s at once."
msgstr "Klik om alle %s te selecteren."

msgid "Choose"
msgstr "Kiezen"

msgid "Remove"
msgstr "Verwijderen"

#, javascript-format
msgid "Chosen %s"
msgstr "Gekozen %s"

#, javascript-format
msgid "This is the list of chosen %s. You may remove some by selecting them in the box below and then clicking the \"Remove\" arrow between the two boxes."
msgstr "Dit is de lijst met gekozen %s. Je kan er enkele verwijderen door ze hieronder te selecteren, en vervolgens te klikken op de pijl ‘Verwijderen’, tussen de twee vakken."

msgid "Remove all"
msgstr "Alles verwijderen"

#, javascript-format
msgid "Click to remove all chosen %s at once."
msgstr "Klik om alle gekozen %s te verwijderen."

msgid "%(sel)s of %(cnt)s selected"
msgid_plural "%(sel)s of %(cnt)s selected"
msgstr[0] "%(sel)s van %(cnt)s geselecteerd"
msgstr[1] "%(sel)s van %(cnt)s geselecteerd"

msgid "You have unsaved changes on individual editable fields. If you run an action, your unsaved changes will be lost."
msgstr "U heeft onopgeslagen wijzigingen in invididueel bewerkbare velden. Als u een actie uitvoert, zullen de onopgeslagen wijzigingen verloren gaan."

msgid "You have selected an action, but you haven't saved your changes to individual fields yet. Please click OK to save. You'll need to re-run the action."
msgstr "U heeft een actie geselecteerd, maar uw wijzigingen aan invididuele velden nog niet opgeslagen. Klik op Oké om op te slaan. U zult de actie opnieuw moeten uitvoeren."

msgid "You have selected an action, and you haven't made any changes on individual fields. You're probably looking for the Go button rather than the Save button."
msgstr "U heeft een actie geselecteerd, en u heeft nog geen wijzigingen aan invididuele velden toegebracht. U zoekt waarschijnlijk de knop ‘Gaan’, en niet de knop ‘Opslaan’."

#, javascript-format
msgid "Note: You are %s hour ahead of server time."
msgid_plural "Note: You are %s hours ahead of server time."
msgstr[0] "Let op: u loopt %s uur voor op de servertijd."
msgstr[1] "Let op: u loopt %s uur voor op de servertijd."

#, javascript-format
msgid "Note: You are %s hour behind server time."
msgid_plural "Note: You are %s hours behind server time."
msgstr[0] "Let op: u loopt %s uur achter op de servertijd."
msgstr[1] "Let op: u loopt %s uur achter op de servertijd."

msgid "Now"
msgstr "Nu"

msgid "Choose a Time"
msgstr "Kies een tijdstip"

msgid "Choose a time"
msgstr "Kies een tijdstip"

msgid "Midnight"
msgstr "Middernacht"

msgid "6 a.m."
msgstr "6 uur ’s ochtends"

msgid "Noon"
msgstr "Middag"

msgid "6 p.m."
msgstr "6 uur ’s avonds"

msgid "Cancel"
msgstr "Annuleren"

msgid "Today"
msgstr "Vandaag"

msgid "Choose a Date"
msgstr "Kies een dag"

msgid "Yesterday"
msgstr "Gisteren"

msgid "Tomorrow"
msgstr "Morgen"

msgid "January"
msgstr "Januari"

msgid "February"
msgstr "Februari"

msgid "March"
msgstr "Maart"

msgid "April"
msgstr "April"

msgid "May"
msgstr "Mei"

msgid "June"
msgstr "Juni"

msgid "July"
msgstr "Juli"

msgid "August"
msgstr "Augustus"

msgid "September"
msgstr "September"

msgid "October"
msgstr "Oktober"

msgid "November"
msgstr "November"

msgid "December"
msgstr "December"

msgctxt "one letter Sunday"
msgid "S"
msgstr "Zo"

msgctxt "one letter Monday"
msgid "M"
msgstr "Ma"

msgctxt "one letter Tuesday"
msgid "T"
msgstr "Di"

msgctxt "one letter Wednesday"
msgid "W"
msgstr "Wo"

msgctxt "one letter Thursday"
msgid "T"
msgstr "Do"

msgctxt "one letter Friday"
msgid "F"
msgstr "Vr"

msgctxt "one letter Saturday"
msgid "S"
msgstr "Za"

msgid "Show"
msgstr "Weergeven"

msgid "Hide"
msgstr "Verbergen"

#, javascript-format
msgid "%s app added"
msgid_plural "%s apps added"
msgstr[0] "%s app toegevoegd"
msgstr[1] "%s apps toegevoegd"

msgid "Add"
msgstr "Toevoegen"

#, javascript-format
msgid "Sorry, there was an error deleting it: %s"
msgstr "Sorry, er is een fout opgetreden bij het verwijderen: %s"

msgid "Delete Screenshot"
msgstr "Schermafdruk verwijderen"

msgid "Are you sure you want to delete this screenshot from your package?"
msgstr "Weet u zeker dat u deze schermafdruk uit uw pakket wilt verwijderen?"

msgid "Delete Feature Graphic"
msgstr "Beschrijvende afbeelding verwijderen"

msgid "Are you sure you want to delete the feature graphic from your package?"
msgstr "Weet u zeker dat u de beschrijvende afbeelding uit uw pakket wilt verwijderen?"

msgid "Delete Version"
msgstr "Versie verwijderen"

msgid "Are you sure you want to delete this version from your package?"
msgstr "Weet u zeker dat u deze versie uit uw pakket wilt verwijderen?"

msgid "Confirm"
msgstr "Bevestigen"

msgid "Added"
msgstr "Toegevoegd"

msgid "You can only upload images here."
msgstr "U kunt hier enkel afbeeldingen uploaden."

#, javascript-format
msgid "Uploading %s file..."
msgid_plural "Uploading %s files..."
msgstr[0] "Bezig met uploaden van %s bestand…"
msgstr[1] "Bezig met uploaden van %s bestanden…"

msgid "Try to drag and drop again!"
msgstr "Probeer het opnieuw te verslepen!"
